﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Net.Mail;
using System.IO;
using Umbraco.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using System.Security.Cryptography;
using System.Text;
using hospitality.Models;


namespace hospitality.Controllers.Surface_Controllers
{
    public class CommentsSurfaceController : Umbraco.Web.Mvc.SurfaceController
    {

        public ActionResult _BlogListComments()
        {
            var viewModel = new BlogCommentsViewModel();
            viewModel.Headline = CurrentPage.GetPropertyValue<string>("postTitle");
            viewModel.Subheading = CurrentPage.GetPropertyValue<string>("postSubheading");
            viewModel.Summary = CurrentPage.GetPropertyValue<string>("postSummary");


            return PartialView("_BlogListComments", viewModel);

        }


        //============ create comment from form postback ======================================================
        [HttpPost]
        [ActionName("BlogListComments")]
        [ValidateAntiForgeryToken]
        public ActionResult BlogListComments(BlogCommentsViewModel model)
        {

            //-------extract personal form elements -----------
            // var title = model.SelectedTitle;
            var name = model.Name;
            var surname = model.Surname;
            var email = model.Email;
            //======== generate todays date ===========
            DateTime dtNow = DateTime.Now;
            string nowYear = (dtNow.Year).ToString();
            string nowMonth = (dtNow.Month).ToString();
            string nowDay = (dtNow.Day).ToString();
            string adDate = (nowDay + "-" + nowMonth + "-" + nowYear);


            //---------generate unique ID number ------
            //create a random ID no--------------------
            Random RandomClass = new Random();
            int RandomNumber = RandomClass.Next(1000, 90000);
            var invID = (RandomNumber.ToString());

            try
            {
                //model not valid, do not save, but return current Umbraco page

                if (ModelState.IsValid)
                {
                    var currentNode = Umbraco.TypedContent(UmbracoContext.PageId.GetValueOrDefault());
                    var currentBlogPost = currentNode.Name;
                    var currentBlogID = currentNode.Id;

                    //-------extract comments form elements -----------
                    var comName = model.Name;
                    var comSurname = model.Surname;
                    var comEmail = model.Email;
                    var comText = model.BodyText;


                    var advertID = (comSurname + "/" + invID);



                    //======== create a new comment node using doc type - comment ============

                    // --- fire up content service ---
                    var cs = Services.ContentService;
                    var parentId = 0;

                    // -- node ID of parent node --  
                    parentId = currentBlogID; //HAS to be here! 
                    var content = cs.CreateContent(advertID, parentId, "comment");


                    // --- add standard form elements and create node --
                    //===========add form elements to new advert node ==========

                    content.Name = (advertID);
                    content.SetValue("commentID", advertID);
                    content.SetValue("postID", currentBlogID);
                    content.SetValue("postTitle", currentBlogPost);
                    content.SetValue("commentDate", adDate);
                    content.SetValue("commentText", comText);
                    content.SetValue("emailAddress", comEmail);
                    content.SetValue("CommentSurname", comSurname);
                    content.SetValue("CommentName", comName);



                    //--Save doc--- change this with cs.Save if we want to save doc but not publish it
                    // cs.SaveAndPublishWithStatus(content);
                    cs.Save(content);



                    TempData["InfoMessage"] = "Thank you for submitting a blog comment.";
                    //--- redirect back to original page ---

                    //======= Redirect to relevant page ============
                    // if (PageURL != "")
                    // {
                    //   Response.Redirect(PageURL);

                    // }
                    //  else
                    //  {
                    //   return RedirectToUmbracoPage(2842);

                    // }




                }
            }
            catch
            {
                // send success message
                TempData["InfoMessage"] = "There has been a technical error...Your comment has NOT been posted...";


            }

            return RedirectToCurrentUmbracoPage();

        }

    }

}


