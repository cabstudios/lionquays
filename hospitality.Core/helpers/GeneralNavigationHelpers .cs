﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace hospitality.Core.helpers
{
  public class GeneralNavigationHelpers
    {
        //------------------- get home page content ------------
        public static DynamicPublishedContentList GetHomePageContent(dynamic myUmbracoRoot)
        {
            //Get stuff
            var homepage = myUmbracoRoot.Where("DocumentTypeAlias = \"homePage\"").First();
            return homepage.Children();
        }

        public static IPublishedContent GetSettings(dynamic myUmbracoRoot)
        {
            var siteSettings = myUmbracoRoot.Where("DocumentTypeAlias = \"settings\"").First();
            return siteSettings;
        }

        //------------------------------ get testimonials ---------------------------
        public static IPublishedContent GetTestimonials(dynamic myUmbracoRoot, string documentTypeAlias)
        {
            var containers = myUmbracoRoot.Where("DocumentTypeAlias = \"homePage\"").First();
            var lists = containers.Descendants("testimonials").First();
            //var list = lists.Descendants(documentTypeAlias).First();
            return lists;
        }

        //------------------------------ get room listings ---------------------------
        public static IPublishedContent GetRoomListings(dynamic myUmbracoRoot, string documentTypeAlias)
        {
            var containers = myUmbracoRoot.Where("DocumentTypeAlias = \"homePage\"").First();
            var lists = containers.Descendants("roomsListing").First();
            //var list = lists.Descendants(documentTypeAlias).First();
            return lists;
        }

        //------------------------------ get media gallery ---------------------------
        public static IPublishedContent GetMediaGallery(dynamic myUmbracoRoot, string documentTypeAlias)
        {
            var containers = myUmbracoRoot.Where("DocumentTypeAlias = \"homePage\"").First();
            var lists = containers.Descendants("gallery").First();
            //var list = lists.Descendants(documentTypeAlias).First();
            return lists;
        }

    }
}