﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web.Models;


namespace hospitality.Core.helpers
{
    public class BlogHelpers
    {
        //------------ get blogs ----------
        public static IPublishedContent GetLatestBlogs(dynamic myUmbracoRoot, string documentTypeAlias)
        {
            //Get stuff
            var containers = myUmbracoRoot.Where("DocumentTypeAlias = \"homePage\"").First();
            var lists = containers.Descendants("blogNews").First();
            return lists;

        }

       
        //------------------------------ get blog categories ---------------------------
        public static IPublishedContent GetBlogCategories(dynamic myUmbracoRoot, string documentTypeAlias)
        {
            var containers = myUmbracoRoot.Where("DocumentTypeAlias = \"settings\"").First();
            var lists = containers.Descendants("blogCategories").First();
            //var list = lists.Descendants(documentTypeAlias).First();
            return lists;
        }


        public static DynamicPublishedContentList GetRecentPosts(dynamic currentPage, int take)
        {
            var site = currentPage.Site();
            var blog = site.Descendants("blogNews").FirstOrDefault();
            //var recentBlogs = blog.Descendants();
            var recentBlogs = blog.Descendants().Where("Level == " + 5).OrderBy("datePosted desc");

            return recentBlogs.Take(take);
        }

        public dynamic GetNextPost(dynamic currentPage)
        {
            var site = currentPage.Site();
            var blog = site.Descendants("blogNews").First();
            var currentDate = currentPage.GetPropertyValue<DateTime>("datePosted");
            var blogPost = blog.Descendants("blogPost");
            var nextBlog = blogPost.Where("datePosted > @0", currentDate).FirstOrDefault();
            return nextBlog;
        }

        public dynamic GetPreviousPost(dynamic currentPage)
        {
            var site = currentPage.Site();
            var blog = site.Descendants("blogNews").First();
            var currentDate = currentPage.GetPropertyValue<DateTime>("datePosted");
            var blogPost = blog.Descendants("blogPost");
            var previousBlog = blogPost.Where("datePosted < @0", currentDate).FirstOrDefault();
            return previousBlog;
        }

        //-------------------------- get recent comments -------------------------
        public static DynamicPublishedContentList GetRecentComments(dynamic currentPage, int take)
        {
            var site = currentPage.Site();
            var blog = site.Descendants("blogNews").FirstOrDefault();
            var recentCommentList = blog.Descendants().Where("Level == " + 6).OrderBy("CreateDate desc");


            return recentCommentList.Take(take);
        }

    }
}