﻿/// Copyright (C) 2016 CAB Studios.
/// All Rights Reserved.

namespace hospitality.Core.Models.View
{
    public class CropModel
    {
        public CropModel() { }

        public CropModel(string alias = "", int width = 0, int height = 0, int retinaWidth = 0, int retinaHeight = 0, string filter = "")
        {
            Alias = alias;
            Width = width;
            Height = height;
            RetinaWidth = retinaWidth;
            RetinaHeight = retinaHeight;
            Filter = filter;
        }

        public string Alias { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int RetinaWidth { get; set; }
        public int RetinaHeight { get; set; }
        public string Filter { get; set; }
    }
}