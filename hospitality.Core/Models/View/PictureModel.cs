﻿/// Copyright (C) 2016 CAB Studios.
/// All Rights Reserved.

namespace hospitality.Core.Models.View
{
    public class PictureModel
    {
        public CropModel Desktop { get; set; }
        public CropModel Tablet { get; set; }
        public CropModel Phablet { get; set; }
        public CropModel Mobile { get; set; }
    }
}