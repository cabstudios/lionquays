﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hospitality.Core.Models
{
    public class SocialPost
    {
        public DateTime Created { get; set; }
        public string Message { get; set; }
        public string Picture { get; set; }
        public string URL { get; set; }
        public string Link { get; set; }
        public SocialService Service { get; set; }
    }

    public enum SocialService
    {
        Facebook,
        Twitter
    }
}
