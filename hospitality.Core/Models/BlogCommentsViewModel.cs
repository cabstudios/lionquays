﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace hospitality.Models
{
   public class BlogCommentsViewModel
    {
        public string Headline { get; set; }
        public string Subheading { get; set; }
        public string Summary { get; set; }

        //-------personal details -----------
        
        [Required, Display(Name = "Name")]
        public string Name { get; set; }
        [Required, Display(Name = "Surname")]
        public string Surname { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        [Required, Display(Name = "Enter your comment")]
        public string BodyText { get; set; }


    

    }
}
