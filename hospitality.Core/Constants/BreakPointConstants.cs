﻿/// Copyright (C) 2016 CAB Studios.
/// All Rights Reserved.

namespace hospitality.Core.Constants
{
    public static class BreakPointstConstants
    {
        public const string Desktop = "(min-width: 1100px)";
        public const string Tablet = "(min-width: 768px) and (max-width: 1100px)";
        public const string Phablet = "(min-width: 481px) and (max-width: 767px)";
        public const string Mobile = "(min-width: 1px) and (max-width: 480px)";
    }
}