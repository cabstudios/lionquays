﻿/// Copyright (C) 2016 CAB Studios.
/// All Rights Reserved.
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using hospitality.Core.Models;
using hospitality.Core.Models.View;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using hospitality.Core.Constants;
using System.Web;

namespace hospitality.Core.Extensions
{
    public static class PictureExtensions
    {
        private static IPublishedContent _image;
        private static List<string> urls;

        /// <summary>
        /// Get a HTML picture tag for a given image
        /// </summary>
        /// <param name="image">An IPublishedContent node</param>
        /// <param name="alt">Alt text</param>
        /// <param name="crops">A list of crops to be used</param>
        /// <returns>A HTML picture tag with a list of srcset tags</returns>
        public static HtmlString ToPicture(this IPublishedContent image, string alt, PictureModel crops)
        {
            _image = image;
            urls = new List<string>();
            var builder = new StringBuilder();

            builder.Append("<!--[if IE 9]><video style=\"display:none;\"><![endif]-->");

            if (crops.Desktop != null)
            {
                builder.Append(GetSourceTag(crops.Desktop, BreakPointstConstants.Desktop));
            }
            if (crops.Tablet != null)
            {
                builder.Append(GetSourceTag(crops.Tablet, BreakPointstConstants.Tablet));
            }
            if (crops.Phablet != null)
            {
                builder.Append(GetSourceTag(crops.Phablet, BreakPointstConstants.Phablet));
            }
            if (crops.Mobile != null)
            {
                builder.Append(GetSourceTag(crops.Mobile, BreakPointstConstants.Mobile));
            }

            builder.Append("<!--[if IE 9]></video><![endif]-->");

            /* Fallback Image */
            if (urls.Any())
            {
                builder.Append(GetImageTag(urls.LastOrDefault(), alt));
            }

            /* Return */
            var picture = new TagBuilder("picture");
            picture.InnerHtml = builder.ToString();
            return new HtmlString(picture.ToString());
        }

        /// <summary>
        /// Get a HTML picture tag for a given image
        /// </summary>
        /// <param name="image">An IPublishedContent node</param>
        /// <param name="crops">A list of crops to be used</param>
        /// <returns>A HTML picture tag with a list of source sets</returns>
        public static HtmlString ToPicture(this IPublishedContent image, PictureModel crops)
        {
            return image.ToPicture(string.Empty, crops);
        }

        #region Private Methods
        /// <summary>
        /// Create a picture srcset tag
        /// </summary>
        /// <param name="crop"></param>
        /// <param name="media">Media query</param>
        /// <returns>A HTML picture srcset tag</returns>
        private static string GetSourceTag(CropModel crop, string media)
        {
            var url = GetCropUrl(crop.Alias, crop.Width, crop.Height);
            if (string.IsNullOrWhiteSpace(url)) return string.Empty;

            urls.Add(url);

            if (crop.RetinaWidth > 0 || crop.RetinaHeight > 0)
            {
                url = string.Format("{0}, {1} 2x", url, GetCropUrl(crop.Alias, crop.RetinaWidth, crop.RetinaHeight, crop.Filter));
            }

            var source = new TagBuilder("source");
            source.Attributes.Add("srcset", url);
            if (!string.IsNullOrWhiteSpace(media)) source.Attributes.Add("media", media);
            return source.ToString();
        }

        private static string GetCropUrl(string alias = "", int width = 0, int height = 0, string filter = "")
        {
            var url = string.Empty;

            if (width > 0 && height > 0)
            {
                if (alias == null)
                {
                    url = string.Format("{0}?width={1}&height={2}", _image.Url, width, height);
                }
                else
                {
                    url = _image.GetCropUrl(cropAlias: alias, width: width, height: height, imageCropMode: ImageCropMode.Crop);
                }
            }
            else if (width > 0)
            {
                if (alias == null)
                {
                    url = string.Format("{0}?width={1}", _image.Url, width);
                }
                else
                {
                    url = _image.GetCropUrl(cropAlias: alias, width: width, imageCropMode: ImageCropMode.Crop);
                }
            }
            else if (height > 0)
            {
                if (alias == null)
                {
                    url = string.Format("{0}?height={1}", _image.Url, height);
                }
                else
                {
                    url = _image.GetCropUrl(cropAlias: alias, height: height, imageCropMode: ImageCropMode.Crop);
                }
            }

            if (!string.IsNullOrWhiteSpace(filter))
            {
                url = string.Format("{0}&filter={1}", url, filter);
            }

            return url;
        }

        /// <summary>
        /// Create the fallback/default img tag
        /// </summary>
        /// <param name="src">Image source Url</param>
        /// <param name="alt">Alt text</param>
        /// <returns>A HTML img tag</returns>
        private static string GetImageTag(string src, string alt)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            var img = new TagBuilder("img");
            img.Attributes.Add("src", src);
            img.Attributes.Add("alt", umbracoHelper.Coalesce(alt, _image.GetPropertyValue<string>("altText"), _image.Name));
            return img.ToString();
        }
        #endregion

        public static string ToBackgroundTags(this IPublishedContent image, PictureModel crops)
        {
            _image = image;
            urls = new List<string>();

            if (image == null) return string.Empty;

            var tags = GetDataTags(string.Empty, "desk", crops.Desktop);
            tags = GetDataTags(tags, "tab", crops.Tablet);
            tags = GetDataTags(tags, "phab", crops.Phablet);
            tags = GetDataTags(tags, "mob", crops.Mobile);
            return tags;
        }

        private static string GetDataTags(string tags, string name, CropModel crop)
        {
            tags = GetDataTag(tags, name, crop.Alias, crop.Width, crop.Height, crop.Filter);
            tags = GetDataTag(tags, name, crop.Alias, crop.RetinaWidth, crop.RetinaHeight, crop.Filter, true);
            return tags;
        }

        private static string GetDataTag(string tags, string name, string alias, int width, int height, string filter = "", bool retina = false)
        {
            var url = GetCropUrl(alias, width, height, filter);
            if (!string.IsNullOrWhiteSpace(url))
            {
                tags = string.Format("{0} data-{1}{2}=\"{3}\"", tags, name, retina ? "-retina" : string.Empty, url);
            }
            return tags;
        }
    }
}